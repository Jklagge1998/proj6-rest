# Project 6: Brevet time calculator service

## Project Information

Author: Jackson Klagge

Email: Jklagge1998@gmail.com

Project Goal: Create a simple listing service from project 5 stored in MondgoDB database using Rest structure. 

## Program Overview
To run this program run the command docker-compose up in the MongoDockerAPI or you can use sh run.sh in a bash shell script terminal. Once ran the program will have 3 ports allocated:

* Port 5000: The consumer program that exposes what is stored in our MongoDB. It lists all formats of data from the mongoDB in project 5 using port 5001.
* Port 5001: The RESTful service that exposes what is stored in MongoDB.
* Port 5002: The brevets table from project 5. It is where entries are added into the mongoDB.

## Port 5001
Port 5001 should have the 9 API's.
The first 3 basic API's have JSON format as a default.

* "http://<host:5001>/listAll" should return all open and close times in the database
* "http://<host:5001>/listOpenOnly" should return open times only
* "http://<host:5001>/listCloseOnly" should return close times only

The following 6 API's are two diffirent representaions of the basic three APIs (one in CSV and JSON):

* "http://<host:5001>/listAll/CSV" should return all open and close times in CSV format
* "http://<host:5001>/listOpenOnly/CSV" should return open times only in CSV format
* "http://<host:5001>/listCloseOnly/CSV" should return close times only in CSV format
* "http://<host:5001>/listAll/JSON" should return all open and close times in JSON format
* "http://<host:5001>/listOpenOnly/JSON" should return open times only in JSON format
* "http://<host:5001>/listCloseOnly/JSON" should return close times only in JSON format

Also the last 6 API's have query parameters to get top "k" open and close times. For example:

* "http://<host:5001>/listOpenOnly/csv?top=3" should return top 3 open times only (in ascending order) in CSV format 
* "http://<host:5001>/listOpenOnly/json?top=5" should return top 5 open times only (in ascending order) in JSON format
* "http://<host:5001>/listCloseOnly/csv?top=6" should return top 5 close times only (in ascending order) in CSV format
* "http://<host:5001>/listCloseOnly/json?top=4" should return top 4 close times only (in ascending order) in JSON format

## How to test functionality
The user can test the programs functionality by going to localhost:5002, after starting the program, and can input entries into the database. They can then go to localhost:5000 and see the format listed. They can also go to the API's above and test functionality(see Port 5001 for detals of expected outcomes). All functionalities should work and have been tested by myself.

## Project Assumptions/ Notes
Assumptions Made for the Brevet Table:

* As in the last project (proj4-brevets) I made the assumption that the controle times will be decided on a hard cutoff. For example if the user enters 200 km acp_times.py will calculate the open and close times according to the second, not first, column of the table. The table can be found in times_acp.py. It is also expected that the user will input inbetween the range of 0 and 1300 km.

Notes about laptop-service:

* In api.py I set the limit for database retrival to 20(the size of the brevets table). This means at most the api will retrieve the top 20 entries. If the user wants a higher limit they need to change the variable top.  