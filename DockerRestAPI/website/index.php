<html>
    <head>
        <title>CIS 322 REST-api demo: Laptop list</title>
    </head>

    <body>
        <h1>All Brevet Time formats</h1>
        <ul>
            <?php
            $listAll = file_get_contents('http://laptop-service/listAll');
            $obj = json_decode($listAll);
	    $kms = $obj->kms;
        $open_times = $obj->open_times;
        $close_times = $obj->close_times;
        echo "control points<br>";
            foreach ($kms as $km) {
        echo "<li>$km</li>";
            }
        echo "open times<br>";
            foreach ($open_times as $ot) {
        echo "<li>$ot</li>";
            }
        echo "close times<br>";
            foreach ($close_times as $ct) {
                echo "<li>$ct</li>";
            }
            
        echo "List Open Times only<br>\n";
            $listOpenOnly = file_get_contents('http://laptop-service/listOpenOnly');
            $obj = json_decode($listOpenOnly);
            $kms = $obj->kms;
            $open_times = $obj->open_times;
            echo "control points<br>";
            foreach ($kms as $km) {
        echo "<li>$km</li>";
            }
        echo "open times<br>";
            foreach ($open_times as $ot) {
        echo "<li>$ot</li>";
            }
        echo "List Close Times only<br>\n";
            $listCloseOnly = file_get_contents('http://laptop-service/listCloseOnly');
            $obj = json_decode($listCloseOnly);
            $kms = $obj->kms;
            $close_times = $obj->close_times;
            echo "control points<br>\n";
            foreach ($kms as $km) {
        echo "<li>$km</li>";
            }
        echo "Close Times<br>\n";
            foreach ($close_times as $ct) {
        echo "<li>$ct</li>";
            }

        echo "List All Times in JSON<br>\n";
            $listAllJSON = file_get_contents('http://laptop-service/listAll/JSON');
            $obj = json_decode($listAllJSON);
        $kms = $obj->kms;
        $open_times = $obj->open_times;
        $close_times = $obj->close_times;
        echo "control points<br>";
            foreach ($kms as $km) {
        echo "<li>$km</li>";
            }
        echo "open times<br>";
            foreach ($open_times as $ot) {
        echo "<li>$ot</li>";
            }
        echo "close times<br>";
            foreach ($close_times as $ct) {
                echo "<li>$ct</li>";
            }
        echo "List All Open Times in JSON<br>\n";
            $listOpenOnlyJSON = file_get_contents('http://laptop-service/listOpenOnly/JSON');
            $obj = json_decode($listOpenOnlyJSON);
            $kms = $obj->kms;
            $open_times = $obj->open_times;
            echo "control points<br>";
            foreach ($kms as $km) {
        echo "<li>$km</li>";
            }
        echo "open times<br>";
            foreach ($open_times as $ot) {
        echo "<li>$ot</li>";
            }

        echo "List Only Close Times in JSON<br>\n";
            $listCloseOnlyJSON = file_get_contents('http://laptop-service/listCloseOnly/JSON');
            $obj = json_decode($listCloseOnlyJSON);
            $kms = $obj->kms;
            $close_times = $obj->close_times;
            echo "control points<br>\n";
            foreach ($kms as $km) {
        echo "<li>$km</li>";
            }
        echo "Close Times<br>\n";
            foreach ($close_times as $ct) {
        echo "<li>$ct</li>";
            }
        
        echo "List All times CSV<br>\n";
        echo file_get_contents("http://laptop-service/listAll/CSV");
        echo "<br>\n";

        echo "List Open Times CSV<br>\n";
        echo file_get_contents(("http://laptop-service/listOpenOnly/CSV"));
        echo "<br>\n";

        echo "List Close TImes CSV<br>\n";
        echo file_get_contents("http://laptop-service/listCloseOnly/CSV");
        echo "<br>\n";

            ?>
        </ul>
    </body>
</html>
