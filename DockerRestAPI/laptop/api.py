# Laptop Service
from flask import Flask, request
from flask_restful import Resource, Api
from pymongo import MongoClient
import pymongo
import flask
import os

# Instantiate the app
app = Flask(__name__)
api = Api(app)

# Gain acess to mongo db
client = MongoClient('db', 27017)
db = client.tododb


class listAll(Resource):
	def get(self):
		#Note: use.limit when looking for queries
		_items = db.tododb.find().sort('km', pymongo.ASCENDING)
		items = [item for item in _items]
		km = []
		open_times = []
		close_times = []
		for item in items:
			km.append(item["km"])
			open_times.append(item["open_time"])
			close_times.append(item["close_time"])
		JSONentry = {}
		JSONentry["kms"] = km
		JSONentry["open_times"] = open_times
		JSONentry["close_times"] = close_times
		return JSONentry

class listOpenOnly(Resource):
	def get(self):
		_items = db.tododb.find().sort('km', pymongo.ASCENDING)
		items = [item for item in _items]
		km = []
		open_times = []
		for item in items:
			km.append(item["km"])
			open_times.append(item["open_time"])
		JSONentry = {}
		JSONentry["kms"] = km
		JSONentry["open_times"] = open_times
		return JSONentry

class listCloseOnly(Resource):
	def get(self):
		_items = db.tododb.find().sort('km', pymongo.ASCENDING)
		items = [item for item in _items]
		km = []
		close_times = []
		for item in items:
			km.append(item["km"])
			close_times.append(item["close_time"])
		JSONentry = {}
		JSONentry["kms"] = km
		JSONentry["close_times"] = close_times
		return JSONentry

class listAllJSON(Resource):
	def get(self):
		top = request.args.get('top')
		if top == None:
			top = 20
		_items = db.tododb.find().sort('km', pymongo.ASCENDING).limit(int(top))
		items = [item for item in _items]
		km = []
		open_times = []
		close_times = []
		for item in items:
			km.append(item["km"])
			open_times.append(item["open_time"])
			close_times.append(item["close_time"])
		JSONentry = {}
		JSONentry["kms"] = km
		JSONentry["open_times"] = open_times
		JSONentry["close_times"] = close_times
		return JSONentry

class listOpenOnlyJSON(Resource):
	def get(self):
		top = request.args.get('top')
		if top == None:
			top = 20
		_items = db.tododb.find().sort('km', pymongo.ASCENDING).limit(int(top))
		items = [item for item in _items]
		km = []
		open_times = []
		for item in items:
			km.append(item["km"])
			open_times.append(item["open_time"])
		JSONentry = {}
		JSONentry["kms"] = km
		JSONentry["open_times"] = open_times
		return JSONentry

class listCloseOnlyJSON(Resource):
	def get(self):
		top = request.args.get('top')
		if top == None:
			top = 20
		_items = db.tododb.find().sort('km', pymongo.ASCENDING).limit(int(top))
		items = [item for item in _items]
		km = []
		close_times = []
		for item in items:
			km.append(item["km"])
			close_times.append(item["close_time"])
		JSONentry = {}
		JSONentry["kms"] = km
		JSONentry["close_times"] = close_times
		return JSONentry

class listAllCSV(Resource):
	def get(self):
		top = request.args.get('top')
		if top == None:
			top = 20
		_items = db.tododb.find().sort('km', pymongo.ASCENDING).limit(int(top))
		items = [item for item in _items]
		csv = "(openTime, CloseTime): "
		for item in items:
			csv += "({}, ".format(item["open_time"])
			csv += "{}), ".format(item["close_time"])
		return csv

class listOpenOnlyCSV(Resource):
	def get(self):
		top = request.args.get('top')
		if top == None:
			top = 20
		_items = db.tododb.find().sort('km', pymongo.ASCENDING).limit(int(top))
		items = [item for item in _items]
		csv = "openTime: "
		for item in items:
			csv += "{}, ".format(item["open_time"])
		return csv

class listCloseOnlyCSV(Resource):
	def get(self):
		top = request.args.get('top')
		if top == None:
			top = 20
		_items = db.tododb.find().sort('km', pymongo.ASCENDING).limit(int(top))
		items = [item for item in _items]
		csv = "CloseTime: "
		for item in items:
			csv += "{}, ".format(item["close_time"])
		return csv

"""
class Laptop(Resource):
    def get(self):
        return {
            'Laptops': ['Mac OS', 'Dell', 
            'Windozzee',
	    'Yet another laptop!',
	    'Yet yet another laptop!'
            ]
        }

# Create routes
# Another way, without decorators
api.add_resource(Laptop, '/')
"""

api.add_resource(listAll, '/listAll')
api.add_resource(listOpenOnly, '/listOpenOnly')
api.add_resource(listCloseOnly, '/listCloseOnly')
api.add_resource(listAllJSON, '/listAll/JSON')
api.add_resource(listOpenOnlyJSON, '/listOpenOnly/JSON')
api.add_resource(listCloseOnlyJSON, '/listCloseOnly/JSON')
api.add_resource(listAllCSV, "/listAll/CSV")
api.add_resource(listOpenOnlyCSV, "/listOpenOnly/CSV")
api.add_resource(listCloseOnlyCSV, "/listCloseOnly/CSV")
# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
